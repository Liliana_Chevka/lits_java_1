package edu.lits.service;

import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;
import edu.lits.pojo.Product;

import java.util.List;

public interface OrderToProductService {

    OrderToProduct saveOrUpdate(OrderToProduct orderToProduct);

    OrderToProduct getByOrderAndProduct(Order currentOrder, Product product);

    void delete(OrderToProduct orderToProduct);

    List<OrderToProduct> getByOrderId(Integer orderId);



}
