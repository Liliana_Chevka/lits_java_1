package edu.lits.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    void store(MultipartFile image, String location);

    Resource load(String imageLocation);

    boolean delete(String imageLocation);
}
