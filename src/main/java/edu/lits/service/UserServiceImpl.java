package edu.lits.service;

import edu.lits.dal.CrudOperationsImpl;
import edu.lits.dal.UserDal;
import edu.lits.model.UserModel;
import edu.lits.pojo.Role;
import edu.lits.pojo.User;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class  UserServiceImpl extends CrudOperationsImpl<User> implements UserService {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    private UserDal userDal;

    @Autowired
    private StorageService storageService;

    public UserServiceImpl(final SessionFactory sessionFactory, PasswordEncoder passwordEncoder) {
        super(User.class, sessionFactory);
        this.passwordEncoder = passwordEncoder;
    }

    public void setUserDal(UserDal userDal) {
        this.userDal = userDal;
    }

    public UserDal getUserDal() {
        return userDal;
    }



    @Override
    public User saveOrUpdate(User user){
        return userDal.saveOrUpdate(user);
    }

    @Override
    public void delete(User user) {
        userDal.delete(user);
    }

    @Override
    public User read(Integer id) {
        return userDal.read(id);
    }

    /*@Override
    @Transactional
    public void removeUser(User user) {
        this.userDalImpl.delete(user);

    }*/

   /* @Override
    @Transactional
    public User getUserById(int id) {
        return this.userDal.getUserById(id);
    }

    @Override
    @Transactional
    public void addUser(User user) {
      this.userDal.addUser(user);
    }

    @Override
    @Transactional
    public List<User> listUser() {
        return this.userDal.userList();
    }*/

   /* @Override
    public void deleteCurrentUser(Integer userId, UserModel userModel) {
        Order currentOrder = orderDal.getCurrentOrder(userId);
        User currentUser = userDal.getCurrentUser(userId);
        Product product = productService.read(newProductModel.getProductId());



        OrderToProduct orderToProduct = orderToProductService.getByOrderAndProduct(currentOrder, product);


        orderToProductService.delete(orderToProduct);
    }*/

    @Override
    public User save(UserModel userModel) {
        String hashedPassword = passwordEncoder.encode(userModel.getPassword());
        User user = new User();
        user.setName(userModel.getName());
        user.setEmail(userModel.getEmail());
        user.setPassword(hashedPassword);
        //todo: remove when decide with roles
        Role role = new Role();
        role.setId(1);
        user.setRole(role);
        return userDal.saveOrUpdate(user);
    }

    @Override
    public User getByEmail(String email) {
        return userDal.getByEmail(email);
    }

    @Override
    public List<User> findAllCustomers() {
        List<User> customers = userDal.findAllCustomers();
        return customers;
    }

    @Override
    public void saveImage(UserModel userModel, User user) {
        if (userModel.getImage().getSize() > 0) {
            String extension = FilenameUtils.getExtension(userModel.getImage().getOriginalFilename());
            user.setImageExtension(extension);
        }
        userDal.saveOrUpdate(user);
        if (userModel.getImage().getSize() > 0) {
            String fileSeparator = System.getProperty("file.separator");
            String imageLocation = fileSeparator + "users" + fileSeparator + user.getId();
            storageService.store(userModel.getImage(), imageLocation);
        }
    }

    @Override
    public Resource loadImage(Integer id) {
        Resource result = null;
        User user = read(id);
        if (user.getImageExtension() != null) {
            String fileSeparator = System.getProperty("file.separator");
            String imageLocation = fileSeparator + "users" + fileSeparator + id + "." + user.getImageExtension();
            result = storageService.load(imageLocation);
        }
        return result;
    }
}
