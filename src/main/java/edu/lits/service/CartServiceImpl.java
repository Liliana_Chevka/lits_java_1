package edu.lits.service;

import edu.lits.dal.CartDal;
import edu.lits.pojo.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

public class CartServiceImpl implements CartService {
    @Autowired(required = true)
    @Qualifier(value = "cartDal")
    private CartDal cartDal;

    public void setCartDal(CartDal cartDal) {
        this.cartDal = cartDal;
    }

    public CartDal getCartDal() {
        return cartDal;
    }


    @Override
    public List<Cart> getCartList() {
//        List<edu.lits.pojo.Cart> list = cartDal.cartList();
//        return list;
        return null;
    }
}
