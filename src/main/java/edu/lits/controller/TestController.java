package edu.lits.controller;

import edu.lits.model.UserModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

    @ResponseBody
    @RequestMapping(value="/greeting",method = RequestMethod.GET)
    public Object Greeting(){
        UserModel userModel = new UserModel();
        userModel.setName("Liliana");
        userModel.setId(1);
        userModel.setEmail("liliana@gmail.com");
        return "Hello";
    }

    @RequestMapping(value="/test",method = RequestMethod.GET)
    public String home(){
        return "bootstraphelloworld";
    }
}
