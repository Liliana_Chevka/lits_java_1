package edu.lits.controller;

import edu.lits.dal.CartDal;
import edu.lits.dal.OrderDal;
import edu.lits.dal.ProductDal;
import edu.lits.pojo.Cart;
import edu.lits.pojo.Order;
import edu.lits.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/carts")
public class CartController {

    @Autowired
    private OrderDal orderDal;

    @Autowired
    private ProductDal productDal;

    @Autowired
    private CartDal cartDal;

    @ResponseBody
    @GetMapping
    public void dsf() {
        Cart cart = new Cart();
        cart.setNumberOfProducts(4);
        Order read = orderDal.read(1);

        cart.setOrder(read);
//        List<Product> list = productDal.list();
//        cart.setProduct(list.get(0));
       cartDal.saveOrUpdate(cart);

    }
}
