package edu.lits.controller;

import edu.lits.dal.UserDal;
import edu.lits.model.UserModel;
import edu.lits.pojo.User;
import edu.lits.service.OrderService;
import edu.lits.service.UserService;
import edu.lits.service.common.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

	@Autowired
	private OrderService orderService;
	@Autowired
	private CurrentUserService currentUserService;
	@Autowired
	private  UserService userService;

	/*public UserController(OrderService orderService) {
		this.orderService = orderService;

	}*/

	@Autowired
	private UserDal userDal;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String getMainPage(Model model) {
		return "main";
	}

	@RequestMapping(value = "/registration")
	public String getRegistrationPage(UserModel userModel) {
		return "registration";
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView user() {

		return
				new ModelAndView("user"
						, "user"
						, new UserModel());
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(@Valid @ModelAttribute UserModel user, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("userModel", user);
			return "registration";
		} else {
			userService.save(user);
			return "redirect:/login";
		}
	}


	@RequestMapping(value = "/user/orders/view",
			method = RequestMethod.GET)
	public String ordersView (Model model) {


		return "ordersInOrder";
	}

	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public String userList(Model model) {
		List<UserModel> userList = new ArrayList<UserModel>();
		UserModel user1 = new UserModel();
		user1.setId(1);
		user1.setName("name from java");
		userList.add(user1);
		UserModel user2 = new UserModel();
		user2.setId(1);
		user2.setName("name from java");
		userList.add(user2);
		model.addAttribute("userList", userList);
		return "list";
	}
	@GetMapping("/users/{id}")
	public String getEditPage(@PathVariable Integer id, Model model) {
		// add in model user by id

		User user = userService.read(id);

		model.addAttribute("user",user);

		return "editUser";
	}

	@PutMapping("/users/{id}")
	public String updateUser(@PathVariable Integer id, @Valid @ModelAttribute UserModel user, BindingResult bindingResult) {
		// code to update user
		User user1 = userService.read(id);
		user1.setName(user.getName());
		userService.saveOrUpdate(user1);
		return "redirect:/user/list";
	}

	@RequestMapping(value = "/customer/info",  method = RequestMethod.GET)
	public String getCustomerInfo( Model model) {
		Integer userId = currentUserService.securityUser().getUserId();
		User user = userDal.read(userId);
		/*model.addAttribute("userModel",userModel);*/
	/*	User user = userService.read(id);*/

		model.addAttribute("user",user);

		return "customerInfoEdit";
	}
	@GetMapping(value = "/customer/info/edit/{id}")
	public String editCustomerInfo(@PathVariable Integer id, Model model) {
		User user = userService.read(id);

		model.addAttribute("user",user);

		return "customerInfo";
	}

	//todo Liliana: Improve image downloading
	@ResponseBody
	@RequestMapping(value = "/users/{id}/image", method = RequestMethod.GET)
	public ResponseEntity<Resource> getImage(@PathVariable Integer id) {
		Resource resource = userService.loadImage(id);
		String fileName = resource == null ? null : resource.getFilename();
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + fileName + "\"").body(resource);
	}

	@PutMapping( value = "/customer/info/save/{id}")
	public String updateUserProfile(@PathVariable Integer id, @Validated @ModelAttribute UserModel user, BindingResult bindingResult) {
		// code to update user
		User user1 = userService.read(id);
		user1.setName(user.getName());
		userService.saveOrUpdate(user1);
		/*userService.saveImage(user, user1);*/
		return "redirect:/customer/info";
	}
	@PostMapping("/customer/info/save/image/{id}")
	public String updateUserImageProfile(@PathVariable Integer id, @Validated @ModelAttribute UserModel user, BindingResult bindingResult) {
		// code to update user
		User user1 = userService.read(id);
		/*user1.setName(user.getName());*/
		/*userService.saveOrUpdate(user1);*/
		userService.saveImage(user, user1);
		return "redirect:/customer/info";
	}

	@DeleteMapping("/users/{id}")
	public String delete(@PathVariable Integer id) {
		// code to delete user
		User user1 = userService.read(id);
		userService.delete(user1);
		System.out.println(id);
		return "redirect:/user/list";
	}
}


