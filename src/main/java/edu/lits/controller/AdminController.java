//package edu.lits.controller;
//
//import edu.lits.dal.ProductDalImpl;
//import edu.lits.model.ProductModel;
//import edu.lits.pojo.Product;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//@Controller
//public class AdminController {
//    @Autowired(required = true)
//    @Qualifier(value="productDal")
//    private ProductDalImpl productDal;
//    ProductModel productModel = new ProductModel();
//
//    // GET: Show product.
////    @RequestMapping(value = {"/admin/product"}, method = RequestMethod.GET)
////    public String detNewProduct(Model model) {
////        Product product = null;
////
//////        if (code != null && code.length() > 0) {
//////            Product product = productDal.findProduct(code);
//////            if (product != null) {
//////                productForm = new ProductForm(product);
//////            }
//////        }
//////        if (productForm == null) {
//////            productForm = new ProductForm();
//////            productForm.setNewProduct(true);
//////        }
//////        model.addAttribute("productForm", productForm);
////        return "AddNewProduct";
////    }
//
//    // POST: Save product
//    @RequestMapping(value = {"/admin/product/save"}, method = RequestMethod.POST)
//    public String productSave(Model model, //
//                              @ModelAttribute("product") @Validated Product product, //
//                              //
//                              final RedirectAttributes redirectAttributes) {
//
//        Product product1= new Product();
//
//        product1.setName(product.getName());
//        product1.setDescription(product.getDescription());
//        product1.setPrice(product.getPrice());
//        model.addAttribute("product", product1);
////        productDal.save(product1);
//
//        return "redirect:/productListInNewOrder";
//    }
//
//}
