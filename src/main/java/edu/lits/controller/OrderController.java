package edu.lits.controller;

import edu.lits.configuration.SecurityUser;
import edu.lits.dal.ProductDal;
import edu.lits.model.NewProductModel;
import edu.lits.model.OrderModel;
import edu.lits.model.ProductInOrderModel;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Order;
import edu.lits.pojo.User;
import edu.lits.service.OrderService;
import edu.lits.service.ProductService;
import edu.lits.service.common.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private CurrentUserService currentUserService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductDal productDal;

	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	@RequestMapping(value = "/order/list")
	public String getOrderList(Model model) {

		List<OrderModel> orderModelList = new ArrayList<>();

		OrderModel order1 = new OrderModel();
//		order1.setID(3);
//		order1.setSum(333);

		OrderModel order2 = new OrderModel();
		//	order2.setID(35);
		//order2.setSum(3335);

		orderModelList.add(order1);
		orderModelList.add(order2);

		model.addAttribute("orderList", orderModelList);
		return "orderList";
	}

	@RequestMapping(value = "/order/products", method = RequestMethod.GET)
	public String productList(Model model) {

		Integer userId = currentUserService.securityUser().getUserId();

		List<edu.lits.pojo.Product> list = productDal.readAll();
		List<ProductModel> productModelList = new ArrayList<ProductModel>();

		for (edu.lits.pojo.Product dbProduct : list) {

			ProductModel productModel = new ProductModel();
			productModel.setId(dbProduct.getId());
			productModel.setName(dbProduct.getName());
			productModel.setDescription(dbProduct.getDescription());
			productModel.setPrice(dbProduct.getPrice());
			productModel.setExtension(dbProduct.getImageExtension());
			productModelList.add(productModel);
		}
		model.addAttribute("productModelList", productModelList);
		return "productList";
	}

	@RequestMapping(value = "/newOrder/current/products",
			method = RequestMethod.GET)
	public String newOrder(Model model) {
		Integer userId = currentUserService.securityUser().getUserId();
		List<ProductInOrderModel> products = orderService.getCurrentOrderProducts(userId);
		double total = productService.calculateTotal(products);
		Order currentOrder = orderService.getCurrent(userId);
		model.addAttribute("currentOrder", currentOrder);
		model.addAttribute("products", products);
		model.addAttribute("total", total);
		return "orders/currentOrder";
	}

	@RequestMapping(value = "/orders/current", method = RequestMethod.POST)
	public String addToOrder(@ModelAttribute NewProductModel newProductModel) {
		Integer userId = currentUserService.securityUser().getUserId();
		orderService.addToCurrentOrder(userId, newProductModel);
		return "redirect:/newOrder/current/products";
	}

	@RequestMapping(value = "/orders/current", method = RequestMethod.DELETE)
	public String delete(@ModelAttribute NewProductModel newProductModel) {
		SecurityUser securityUser = currentUserService.securityUser();
		Integer userId = securityUser.getUserId();
		orderService.deleteFromCurrentOrder(userId, newProductModel);
		return "redirect:/newOrder/current/products";
	}

	@RequestMapping(value = "/orders/current", method = RequestMethod.PUT)
	public String edit(@ModelAttribute @Valid NewProductModel newProductModel, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("newProductModel", newProductModel);
		} else {
			Integer userId = currentUserService.securityUser().getUserId();
			orderService.editCurrentOrder(userId, newProductModel);
		}
		return "redirect:/newOrder/current/products";
	}

	@RequestMapping(value = "/orders/current/buy", method = RequestMethod.POST)
	public String buy(Model model) {
		SecurityUser securityUser = currentUserService.securityUser();
		Integer userId = securityUser.getUserId();
		List<ProductInOrderModel> products = orderService.getCurrentOrderProducts(userId);
		Order order = orderService.buy(userId);
		User seller;
		seller = order.getSeller();
		double total = 0;
		for (ProductInOrderModel product : products) {
			total = total + product.getPrice() * product.getCount();
		}
		model.addAttribute("seller",seller);
		model.addAttribute("products", products);
		model.addAttribute("total", total);
		model.addAttribute("currentOrder", order);

		return "orders/currentOrderBuy";
	}
}
