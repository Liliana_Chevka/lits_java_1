package edu.lits.controller;

import edu.lits.dal.UserDal;
import edu.lits.model.ProductModel;
import edu.lits.model.UserModel;
import edu.lits.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ManagerController {
    @Autowired(required=true)
    @Qualifier(value="userDal")
    private UserDal userDal;
    public void setUserDal(UserDal userDalValue){
        this.userDal = userDalValue;
    }

    List<UserModel> userModelList = new ArrayList<>();
    UserModel userModel = new UserModel();

    @RequestMapping(value = "/manager/profile/info",
            method = RequestMethod.GET)
    public String managerProfile(Model model) {

//
//        User user = userDal.get(1);
        model.addAttribute("userModel", userModel);

        return "managerProfile";
    }


    @RequestMapping(value = "/manager/profile/edit",
            method = RequestMethod.GET)
    public String editManagerProfile(Model model, @ModelAttribute UserModel userModel) {
       //todo: Remove hardcoded id after adding authentication
//        User user = userDal.get(1);
//        userModel.setId(user.getId());
//        userModel.setName(user.getName());
        model.addAttribute("userModel", userModel);


        return "managerProfileEdit";
    }




    @RequestMapping(value = "/get/all/user",
            method = RequestMethod.GET)
    public String getAllUser(Model model) {

        List<UserModel> userList = new ArrayList<>();
        UserModel userModel = new UserModel();
        userModel.setId(1);
        userModel.setName("Liliana Chevka");
        userList.add(userModel);
        UserModel userModel2 = new UserModel();
        userModel2.setId(2);
        userModel2.setName("Anya");
        userList.add(userModel2);

        model.addAttribute("userList", userList);


        return "getAllUser";
    }

    @RequestMapping(value = "/manager/profile/save",
            method = RequestMethod.POST)
    public String saveManagerProfile(Model model, @ModelAttribute("userModel") @Validated UserModel userModel, final RedirectAttributes redirectAttributes) {

        User user = new User();
//        todo: Remove hardcoded id after adding authentication
        user.setId(1);
        user.setName(userModel.getName());
//        userDal.save(user);
//        model.addAttribute("userModel",userModel);

        return "managerProfile";
    }


    @RequestMapping(value = "/data/list",
            method = RequestMethod.GET)
    public String listsView(Model model) {
        List<ProductModel> productModelList = new ArrayList<ProductModel>();

        model.addAttribute("productModelList", productModelList);
        ProductModel product2 = new ProductModel();
        product2.setId(2);
        product2.setName("product2");
        product2.setPrice(222);

        ProductModel product3 = new ProductModel();
        product3.setId(3);
        product3.setName("product3");
        product3.setPrice(333);


        productModelList.add(product2);
        productModelList.add(product3);


        return "Lists";
    }
}


