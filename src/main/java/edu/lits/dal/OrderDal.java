package edu.lits.dal;

import edu.lits.model.ProductInOrderModel;
import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;

import java.util.List;

public interface OrderDal extends CrudOperations<Order> {

    Order getCurrentOrder(Integer userId);

    List<ProductInOrderModel> getCurrentOrderProducts(Order currentOrder);
    List<Order> ordersForCustomer(Integer id);

}
