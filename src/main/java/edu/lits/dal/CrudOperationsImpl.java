package edu.lits.dal;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CrudOperationsImpl<T> implements CrudOperations<T> {

    private final Class<T> type;
    private final SessionFactory sessionFactory;

    public CrudOperationsImpl(Class<T> type, SessionFactory sessionFactory) {
        this.type = type;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public T read(Integer id) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        T entity = session.get(type, id);
        tx.commit();
        session.close();
        return entity;
    }

    @Override
    public T saveOrUpdate(T entity) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(entity);
        tx.commit();
        session.close();
        return entity;
    }

    @Override
    public List<T> readAll() {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(type);
        query.from(type);
        List<T> resultList = session.createQuery(query).getResultList();
        tx.commit();
        session.close();
        return resultList;
    }

    @Override
    public void delete(T entity) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(entity);
        tx.commit();
        session.close();
    }


}
