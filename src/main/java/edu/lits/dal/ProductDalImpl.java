package edu.lits.dal;

import edu.lits.pojo.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public class ProductDalImpl extends CrudOperationsImpl<Product> implements ProductDal {

	//@Autowired
	private SessionFactory sessionFactory;

	public ProductDalImpl(SessionFactory sessionFactory) {
		super(Product.class, sessionFactory);
		this.sessionFactory = sessionFactory;
	}
}
