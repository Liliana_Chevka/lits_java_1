package edu.lits.dal;

import edu.lits.model.ProductInOrderModel;
import edu.lits.pojo.Order;
import edu.lits.pojo.OrderStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderDalImpl extends CrudOperationsImpl<Order> implements OrderDal {

    private SessionFactory sessionFactory;

    public OrderDalImpl(SessionFactory sessionFactory) {
        super(Order.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Order getCurrentOrder(Integer userId) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Order order = (Order) session.createQuery("select o from edu.lits.pojo.Order o" +
                " where o.customer.id = :userId and " +
                "o.status = :status")
                .setParameter("userId", userId)
                .setParameter("status", OrderStatus.PREPARE)
                .uniqueResult();
        tx.commit();
        session.close();
        return order;
    }

    @Override
    public List<ProductInOrderModel> getCurrentOrderProducts(Order currentOrder) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<ProductInOrderModel> products = (List<ProductInOrderModel>) session.createQuery("select " +
                "new edu.lits.model.ProductInOrderModel(p.id, p.name, p.price, otp.productCount) " +
                "from edu.lits.pojo.Product p" +
                " join p.orders otp " +
                "where otp.order = :order")
                .setParameter("order", currentOrder)
                .list();
        tx.commit();
        session.close();
        return products;
    }
    @Override
    public List<Order> ordersForCustomer(Integer id) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Order> orders = session.createQuery("select distinct o from Order as o left join fetch o.products otp join fetch otp.product where o.customer.id=:customer_id" )
                .setParameter("customer_id", id)
                .list();
        tx.commit();
        session.close();
        return orders;


    }
}
