package edu.lits.model;

public class OrderPriceModel {

    private OrderModel orderModel;
    private double total;

    public OrderPriceModel(OrderModel orderModel, double total) {
        this.orderModel = orderModel;
        this.total = total;
    }

    public OrderModel getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(OrderModel orderModel) {
        this.orderModel = orderModel;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
