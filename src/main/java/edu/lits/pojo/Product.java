package edu.lits.pojo;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@Column(name = "id", unique = /**/true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int id;

	private String name;

	private String description;

	private Integer price;

	private Integer quantity;

	@Column(name = "image_extension")
	private String imageExtension;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrderToProduct> orders;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public List<OrderToProduct> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderToProduct> orders) {
		this.orders = orders;
	}

	public void setQuantity(Integer newQuantity){
		quantity = newQuantity;
	}
	public Integer getQuantity(){
		return quantity;
	}

	@Override
	public String toString() {
		return "Product{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", price=" + price +
				'}';
	}

	public String getImageExtension() {
		return imageExtension;
	}

	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
}
