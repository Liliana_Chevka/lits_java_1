import lesson.stream.Book;
import lesson.stream.Worker;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class FirstUnitTest {

	Worker worker;

	@Before
	public void beforeTest() {
		worker = new Worker();
		System.out.println("before");
	}

	@Test
	public void testWorker() {

		List<Book> listBook = worker.getListBook();

		Assert.assertTrue(listBook.size()==1);
	}

	@Test
	public void testWorker2() {

		List<Book> listBook = worker.getListBook();

		Assert.assertTrue(listBook.size()==1);
	}

	@Test
	public void testWorkerFirstBook() {
		int value = worker.getListBook().get(0).getFieldI();

		Assert.assertTrue(value==1);

		worker.getListBook().add(new Book("",1d,2));
	}

	@BeforeClass
public static void beforeAll() {
	System.out.println("only once");
}


}
